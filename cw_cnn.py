#------------------------------------------------------------------------------------------
# FILE: train_cwcnn.py
# Trains a CNN Model to predict window center and width of a mammogram given its image:
#------------------------------------------------------------------------------------------

from __future__ import division
import sys,os

sys.path.append(os.path.abspath('/share/jonathan/deep_models/models/research/slim/nets'))
sys.path.append(os.path.abspath('..'))

import tensorflow as tf
import tensorflow.contrib.slim as slim
from nets.inception_resnet_v2 import inception_resnet_v2, inception_resnet_v2_arg_scope

import numpy as np
import h5py
import argparse
import threading
import time
import pydicom
import cv2

from dicom_to_hdf5 import create_dataset
from evaluation import calculate_mean_overlap

#------------------------------------------------------------------------------------------
# FUNCTION: log_output(sess,log_path,dataset_type,images_next,labels_next,x,y,logits,loss,
#                      is_training,iteration,writer)
# OVERVIEW: Logs the performance of a given dataset at a certain state of the trained model
# INPUTS:   sess: Current session of the trained model
#           log_path: Path to place log files
#           dataset_type: Name for dataset to append on log file names (ex. training,validation,test)
#           images_next: 'Next' pointer of image iterator
#           labels_next: 'Next' pointer of labels iterator
#           x: Tensor Placeholder for images
#           y: Tensor Placeholder for labels
#           logits: Prediction tensor for center and width
#           loss: Loss operation
#           is_training: Training mode boolean tensor placeholder
#           iteration: Iteration number
#           writer: Summary writer to tensorboard
# OUTPUTS: Creates log files containing inference and performance information at given iteration of model
#          Under the logs folder.
#------------------------------------------------------------------------------------------

def log_output( sess,
                log_path,
                dataset_type,
                images_next,
                labels_next,
                x,
                y,
                logits,
                loss,
                is_training,
                iteration,
                writer):

    data_loss_so_far = 0.0
    predictions_combined = np.empty((0,2),np.float32)
    labels_combined = np.empty((0,2),np.float32)
    data_size = 0
    try:
        while True:
            batch_images,batch_label = sess.run([images_next,labels_next])
            batch_loss,pred = sess.run([loss,logits],feed_dict={x:batch_images,y:batch_label,
                                                        is_training:False})
            pred = (pred*127.5)+127.5

            data_loss_so_far += len(batch_images)*batch_loss

            with open(os.path.join(log_path,'prediction_'+dataset_type+'_'+str(iteration)+'.txt'),'a') as f:
                for k in range(len(pred)):
                    f.write(str(pred[k][0]) + ',' + str(pred[k][1]) + ',')
                    f.write(str(batch_label[k][0]) + ',' + str(batch_label[k][1]) + '\n')
            predictions_combined = np.vstack((predictions_combined,pred))
            labels_combined = np.vstack((labels_combined,batch_label))
            data_size += len(batch_images)

    except tf.errors.OutOfRangeError:
        pass
    dataset_loss = data_loss_so_far/data_size
    print('Epoch: %d, Average %s Dataset Loss: %f' % (iteration,dataset_type.capitalize(),dataset_loss))

    with open(os.path.join(log_path,'overlap_'+dataset_type+'.txt'),'a') as f:
        overlap_string = str(iteration)+','
        try:
            mean_overlap = calculate_mean_overlap(predictions_combined,labels_combined)
            summary_overlap = tf.Summary(value=[tf.Summary.Value(tag=dataset_type+'_overlap',simple_value=mean_overlap)])
            writer.add_summary(summary_overlap,iteration)
            overlap_string += str(mean_overlap)
        except ValueError as e:
            overlap_string += str(e)
        overlap_string += '\n'
        f.write(overlap_string)
        print('Epoch %d %s Overlap: %s' % (iteration,dataset_type.capitalize(),overlap_string.strip().split(',')[1]))

    summary_loss = tf.Summary(value=[tf.Summary.Value(tag=dataset_type+'_loss',simple_value=dataset_loss)])
    writer.add_summary(summary_loss,iteration)



#------------------------------------------------------------------------------------------
# FUNCTION: inception_resnet_resize(reg_constant,batch_decay)
# OVERVIEW: Defines the Inception Resnet V2 Neural Network Architecture and directly
#           resizes input images to 299x299 dimensions to fit into network
#
# INPUTS:
#   reg_constant: The lambda value used for Regularization
#   batch_decay: The batch normalization decay
# OUTPUT:
#   x: Placeholder to feed input images
#   y: Placeholder to feed output images
#   normalized_y: Tensor that provides normalized labels between -1 and 1
#   logits: Output tensor providing the predicted window center and width
#   is_training: Placeholder to set training mode
#------------------------------------------------------------------------------------------
def inception_resnet_resize(reg_constant=0.00004,
                            batch_decay=0.95):

    x = tf.placeholder(shape=[None,1196,1196],dtype=tf.float32,name='input')
    stacked_x = tf.stack([x,x,x],axis=3,name='stack')
    resized_x = tf.image.resize_images(stacked_x,(299,299))

    y = tf.placeholder(shape=[None,2],dtype=tf.float32,name='labels')
    mid_8_bit = tf.constant(127.5,dtype=tf.float32)
    normalized_y = tf.divide(tf.subtract(y,mid_8_bit),mid_8_bit)

    is_training = tf.placeholder(shape=None, dtype=tf.bool,name='is_training')

    with slim.arg_scope(inception_resnet_v2_arg_scope(weight_decay=reg_constant,
                                                      batch_norm_decay=batch_decay)):
		logits, end_points = inception_resnet_v2(resized_x,
							num_classes=2,
							is_training=is_training,
                            dropout_keep_prob=1.0)
    return (x,y,normalized_y,logits,is_training)

#------------------------------------------------------------------------------------------
# FUNCTION: inception_resnet_two_layer_resize(reg_constant,batch_decay)
# OVERVIEW: Defines the Inception Resnet V2 Neural Network Architecture + two additional
#           convolution layers before its input layer to learn how to resize image
#
# INPUTS:
#   reg_constant: The lambda value used for Regularization
#   batch_decay: The batch normalization decay
# OUTPUT:
#   x: Placeholder to feed input images
#   y: Placeholder to feed output images
#   normalized_y: Tensor that provides normalized labels between -1 and 1
#   logits: Output tensor providing the predicted window center and width
#   is_training: Placeholder to set training mode
#------------------------------------------------------------------------------------------

def inception_resnet_two_layer_resize(reg_constant=0.00004,batch_decay=0.95):

    x = tf.placeholder(shape=[None,1196,1196],dtype=tf.float32,name='input')
    stacked_x = tf.stack([x,x,x],axis=3,name='stack')
    l2_regularizer = slim.l2_regularizer(reg_constant)

    conv1 = tf.layers.conv2d(inputs=stacked_x,
            filters=3,
            kernel_size=[3,3],
            strides=2,
            padding='same',
            activation=tf.nn.relu,
            kernel_regularizer=l2_regularizer,
            bias_regularizer=l2_regularizer,
            name='conv1')

    conv2 = tf.layers.conv2d(inputs=conv1,
            filters=3,
            kernel_size=[3,3],
            strides=2,
            padding='same',
            activation=tf.nn.relu,
            kernel_regularizer=l2_regularizer,
            bias_regularizer=l2_regularizer,
            name='conv2')

    y = tf.placeholder(shape=[None,2],dtype=tf.float32,name='labels')
    mid_8_bit = tf.constant(127.5,dtype=tf.float32)
    normalized_y = tf.divide(tf.subtract(y,mid_8_bit),mid_8_bit)

    is_training = tf.placeholder(shape=None, dtype=tf.bool,name='is_training')

    l2_regularizer = slim.l2_regularizer(reg_constant)

    with slim.arg_scope(inception_resnet_v2_arg_scope(weight_decay=reg_constant,
                                                      batch_norm_decay=batch_decay)):
		logits, end_points = inception_resnet_v2(conv2,
							num_classes=2,
							is_training=is_training,
                            dropout_keep_prob=1.0)

    return (x,y,normalized_y,logits,is_training)

#------------------------------------------------------------------------------------------
# FUNCTION: train_model(train_data,
#                       validation_data,
#                       experiment_name,
#                       direct_resize,
#                       pretrained_model_path,
#                       gpu_num,
#                       batch_size,
#                       learning_rate,
#                       epochs,
#                       test_data,
#                       reg_constant,
#                       batch_decay,
#                       log_frequency,
#                       model_save_frequency
#                       )
#
# OVERVIEW: Trains network with given hyperparameters
#           Will create two folders: logs and models which will store the state information
#           of the model at a given frequency.
# INPUTS:
#   train_data: Training Set HDF5 file path
#   validation_data: Validation Set HDF5 file path
#   experiment_name: Experiment Name used
#   direct_resize: Boolean that selects which architecture to use defined in the functions above
#   pretrained_model_path: Inception Resnet V2 Checkpoint Path
#   gpu_num: Number corresponding to the GPU for use
#   test_data (Optional): Test Set HDF5 file path (Default = '')
#   batch_size (Optional): Size of batches to run through the network during training (Default = 1)
#   learning_rate (Optional): Learning rate (Default = 0.00005)
#   epochs (Optional): Maximum number of Epochs (Default = 1000)
#   reg_constant (Optional): The lambda value used for Regularization (Default = 0.00004)
#   batch_decay (Optional): The batch normalization decay (Default = 0.95)
#   log_frequency (Optional): The number of epochs before each logging occurs (Default = 5)
#   model_save_frequency (Optional): The number of epochs before the saving of each model (Default = 250)
# OUTPUT: None
#------------------------------------------------------------------------------------------
def train_model(train_data,
                validation_data,
                experiment_name,
                direct_resize,
                pretrained_model_path,
                gpu_num,
                batch_size=1,
                learning_rate=0.00005,
                epochs=1000,
                reg_constant=0.00004,
                batch_decay=0.95,
                log_frequency=5,
                model_save_frequency=250):

    os.environ['CUDA_VISIBLE_DEVICES'] = gpu_num
    log_path = os.path.join('logs',experiment_name+'_'+str(learning_rate))
    model_path = os.path.join('models',experiment_name+'_'+str(learning_rate))

    if not os.path.exists(log_path):
        os.makedirs(log_path)

    if not os.path.exists(model_path):
        os.makedirs(model_path)

    train_images_iter,train_images_next,train_labels_iter,train_labels_next = create_dataset(train_data,batch_size)
    validation_images_iter,validation_images_next,validation_labels_iter,validation_labels_next = create_dataset(validation_data,batch_size)

    if direct_resize:
        print('Direct Resize')
        x,y,normalized_y,logits,is_training = inception_resnet_resize()
    else:
        print('Two Layer Resize')
        x,y,normalized_y,logits,is_training = inception_resnet_two_layer_resize()


    variables_to_restore = slim.get_variables_to_restore(exclude=['InceptionResnetV2/Logits',
                                                                'InceptionResnetV2/AuxLogits',
                                                                'conv1',
                                                                'conv2'])

    reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
    loss = tf.losses.mean_squared_error(normalized_y,logits) + sum(reg_losses)
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train_op = slim.learning.create_train_op(loss,optimizer)

    saver = tf.train.Saver(variables_to_restore)
    model_saver = tf.train.Saver(tf.global_variables(),max_to_keep=100)

    with tf.Session() as sess:
        sess.run(tf.group(tf.local_variables_initializer(),
                        tf.global_variables_initializer()))
        writer = tf.summary.FileWriter(log_path, sess.graph)
        saver.restore(sess,pretrained_model_path)

        for i in range(epochs):
            sess.run([train_images_iter.initializer,train_labels_iter.initializer])
            try:
                j=0
                while True:
                    batch_images,batch_labels = sess.run([train_images_next,train_labels_next])
                    sess.run(train_op,feed_dict={x:batch_images,y:batch_labels,is_training:True})
                    batch_loss = sess.run(loss,feed_dict={x:batch_images,y:batch_labels,is_training:False})
                    print('Epoch: %d, Batch: %d, Loss: %f' % (i,j,batch_loss))
                    j+=1
            except tf.errors.OutOfRangeError:
                pass

            if i % log_frequency == 0 and i > 0:
                sess.run([train_images_iter.initializer,train_labels_iter.initializer])
                log_output(sess,log_path,'training',train_images_next,train_labels_next,x,y,logits,loss,is_training,i,writer)
                sess.run([validation_images_iter.initializer,validation_labels_iter.initializer])
                log_output(sess,log_path,'validation',validation_images_next,validation_labels_next,x,y,logits,loss,is_training,i,writer)

            if i%model_save_frequency == 0 and i > 0:
                model_saver.save(sess,os.path.join(model_path,'model'), global_step=i)


if __name__ == '__main__':
    	 parser = argparse.ArgumentParser(description='Window Center/Width CNN Model')
         parser.add_argument('-e', dest='run_id',help='Experiment Name',required=True)
         parser.add_argument('-t', dest='train_data',help='HDF5 File of Training Data',required=True)
         parser.add_argument('-v', dest='validation_data',help='HDF5 File of Validation Data',required=True)
         parser.add_argument('-g', dest='gpu_num',help='GPU index', required=True)
         parser.add_argument('-c', dest='pretrained_model_path',help='Path of Pretrained Model Checkpoint', required=True)
         parser.add_argument('-a', dest='direct_resize', help='Direct Resize Architecture (1) or Two Conv Layer Resize Architecture (0)', required=True,type=int)
         parser.add_argument('-i', dest='epochs',help='Number of Epochs',type=int,default=1000)
         parser.add_argument('-r', dest='reg_constant',help='Regularization Constant',type=float,default=0.00004)
         parser.add_argument('-b', dest='batch_size',help='Batch size',default=50,type=int)
         parser.add_argument('-l', dest='learning_rate',help='Learning rate',default=0.001,type=float)
         parser.add_argument('-d', dest='batch_decay', help='Batch Decay',default=0.95,type=float)
         parser.add_argument('-log', dest='log_frequency', help='Log Frequency',default=0.95,type=float)
         parser.add_argument('-s', dest='model_save_frequency', help='Model Save Frequency',default=0.95,type=float)
         args = parser.parse_args()

         train_model(args.train_data,
                    args.validation_data,
                    args.run_id,
                    args.direct_resize,
                    args.pretrained_model_path,
                    args.gpu_num,
                    args.batch_size,
                    args.learning_rate,
                    args.epochs,
                    args.reg_constant,
                    args.batch_decay,
                    args.log_frequency,
                    args.model_save_frequency)
