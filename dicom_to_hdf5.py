#------------------------------------------------------------------------------------------
# FILE: dicom_to_hdf5.py
# Converts a file of dicom paths into an hdf5 file containing:
#   Indices: The line number that corresponds to the dicom path in the inputted file
#   Images: Preprocessed images (resized and rescaled to 256 bit)
#   Labels: Center and width (rescaked to 256 bit)
#------------------------------------------------------------------------------------------

from __future__ import division
import tensorflow as tf
import numpy as np
import h5py
import argparse
import threading
import time
import pydicom
import cv2

#------------------------------------------------------------------------------------------
# FUNCTION: preprocess_data(filepath,index)
# OVERVIEW: Preprocesses and returns image and labels from one dicom path
# INPUTS:
#       filepath: File path of dicom you would like to preprocess
#       count: Line number corresponding to the dicom path in dicom path file
# OUTPUTS:
#       List: [count,image,(window_center,window_width)]
#       image resized and rescaled to 256 bit
#       center and width rescaled to 256 bit
#------------------------------------------------------------------------------------------

def preprocess_data(filepath,index=0):

    print('Image ', index)

    #Read in dicom from path
    dcm = pydicom.dcmread(filepath)

    #Check to see if DICOM is a Mammogram and has annotated image, center and width
    if ( hasattr(dcm,'pixel_array') and
         hasattr(dcm,'WindowCenter') and
         hasattr(dcm,'WindowWidth') and
         dcm.Modality == 'MG'):

        #Resize image to 1196 x 1196
        img = cv2.resize(dcm.pixel_array,(1196,1196),interpolation=cv2.INTER_NEAREST)
        max_pixel = img.max()

        #Takes the first center and width if WindowCenter and WindowWidth are represented as a list
        windowCenter = float(dcm.WindowCenter[0]) if type(dcm.WindowCenter) is pydicom.multival.MultiValue else float(dcm.WindowCenter)
        windowWidth = float(dcm.WindowWidth[0]) if type(dcm.WindowWidth) is pydicom.multival.MultiValue else float(dcm.WindowWidth)

        #Checking if the pixels need to be inverted
        lut_shape = dcm.PresentationLUTShape.lower()
        photo_metric = dcm.PhotometricInterpretation.lower()
        if lut_shape == 'inverse':
            assert photo_metric == 'monochrome1'
            print('Inverting Image')
            img = max_pixel - img

        #Checking what size bits is used to represent the center and width
        bitsStored = dcm.BitsStored
        if bitsStored == 16:
           print('Bitstored is 16')
           #Sanity check to see if actual bits stored is 12 when bits stored is 16
           #When BitsStored is 16, dicom has LargestImagePixelValue that includes
           assert dcm.LargestImagePixelValue == 4095
           bitsStored = 12

        #Rescale down to 255 bits which is normally the input for Inception Resnet
        img = 255*(img/max_pixel)
        windowCenter = 255*(windowCenter/(2**(bitsStored)-1))
        windowWidth = 255*(windowWidth/(2**(bitsStored)-1))

        return [index,img,[windowCenter,windowWidth]]

#------------------------------------------------------------------------------------------
# FUNCTION: store_data(indices,images,labels,output_path)
# OVERVIEW: Stores indices, images and labels into one hdf5 file.
# INPUTS:
#       indices: A list of indices that correspond to the dicom number in the dicom filelist
#                For each image,label pair
#       images: A list of images of the same dimension to store (?,h,w)
#       labels: File path of dicom you would like to preprocess (?,2)
#       output_path: Output hdf5 path
# OUTPUTS: None - Creates an hdf5 file.
#------------------------------------------------------------------------------------------
def store_data(indices,images,labels,output_path):
    image_shape = np.array(images).shape
    hdf5_file = h5py.File(output_path,mode='w')
    hdf5_file.create_dataset('index',(len(images),),np.int32)
    hdf5_file['index'][...] = indices
    hdf5_file.create_dataset('images',image_shape,np.float32)
    hdf5_file['images'][...] = images
    hdf5_file.create_dataset('labels',(len(labels),2),np.float32)
    hdf5_file['labels'][...] = labels
    hdf5_file.close()

#------------------------------------------------------------------------------------------
# FUNCTION: read_data(filepath)
# OVERVIEW: Read hdf5 file from path and return indices, images and labels
# INPUTS: filepath: The path of the hdf5 file you want to read
# OUTPUTS: Three numpy arrays in a list corresponding to indices, images and labels
#------------------------------------------------------------------------------------------

def read_data(filepath):
    hdf5_file = h5py.File(filepath,'r')
    output = [hdf5_file['index'][...],hdf5_file['images'][...],hdf5_file['labels'][...]]
    return output

#------------------------------------------------------------------------------------------
# CLASS: DICOM_HDF5_Converter
# OVERVIEW: Class that handles the preprocessing and storage of all relevant
#           DICOM data in either a multithreaded or single threaded process
#------------------------------------------------------------------------------------------
class DICOM_HDF5_Converter:

    output_data = []

    def __init__(self):
        pass

    #Wrapper function that allows for multithreaded addition of data
    @classmethod
    def preprocess_data(cls,file_path,count):
        cls.output_data.append(preprocess_data(file_path,count))

    @classmethod
    def reset_output(cls):
        cls.output_data = []

    #Multi-threaded Data Extraction + Storage
    @classmethod
    def threaded_preprocess_data(cls,dicom_path,output_dir,num_threads):
        thread_list = []
        thread_target = []

        def f():
            return False

        for i in range(num_threads):
            thread_list.append(threading.Thread(target=f))
            thread_list[-1].start()
            thread_target.append(cls)

        with open(dicom_path) as f:
            file_list = [line.strip() for line in f][:100]
            count = 0
            for line in file_list:
                # wait for open thread:
                i = 0
                while True:
                    if not thread_list[i].is_alive():
                        break
                    i += 1
                    if i >= len(thread_list):
                        i = i % len(thread_list)
                        time.sleep(0.1)
                thread_list[i] = threading.Thread(target=thread_target[i].preprocess_data, args=[line.strip(),count])
                thread_list[i].start()

                print("  Started on thread {}".format(i))
                count+=1

            # Join threads
            for i in range(len(thread_list)):
                thread_list[i].join()

        indices = []
        img = []
        labels = []

        for row in cls.output_data:
            indices.append(row[0])
            img.append(row[1])
            labels.append(row[2])

        store_data(indices,img,labels,output_dir)
        cls.reset_output()

    @classmethod
    def single_threaded_preprocess_data(cls,dicom_path,output_dir):
        with open(dicom_path) as f:
            file_list = [line.strip() for line in f][:100]
            count = 0
            for line in file_list:
                cls.preprocess_data(line.strip(),count)
                count+=1

        indices = []
        img = []
        labels = []

        for row in cls.output_data:
            indices.append(row[0])
            img.append(row[1])
            labels.append(row[2])

        store_data(indices,img,labels,output_dir)
        cls.reset_output()

#------------------------------------------------------------------------------------------
# CLASS: DataGenerator
# OVERVIEW: Creates the generator that allows for lazy return of data
#------------------------------------------------------------------------------------------

class DataGenerator:
    def __init__(self,file):
        self.file = file

    def get_indices(self):
        with h5py.File(self.file, 'r') as hf:
            for index in hf['index']:
                yield index

    def get_images(self):
        with h5py.File(self.file, 'r') as hf:
            for im in hf['images']:
                yield im

    def get_labels(self):
        with h5py.File(self.file,'r') as hf:
            for label in hf['labels']:
                yield label

#------------------------------------------------------------------------------------------
# FUNCTION: create_dataset(data_file,batch_size)
# OVERVIEW: Creates iterators that points to the data into the given HDF5 file.
#           Lazy evaluation saves memory consumption when loading data for training
#
# INPUTS:
#   data_file: HDF5 File Path of Data
#   batch_size: Batch size for iterator
# OUTPUT:
#   images_iter: Iterator for images in HDF5
#   images_next: Iterator pointer to next batch of images
#   labels_iter: Iterator for labels in HDF5
#   labels_next: Iterator pointer to next batch of labels
#------------------------------------------------------------------------------------------

def create_dataset(data_file,batch_size):
    generator = DataGenerator(data_file)
    images = tf.data.Dataset.from_generator(generator.get_images,
                                        tf.float32,
                                        tf.TensorShape([1196,1196])).batch(batch_size)
    images_iter = images.make_initializable_iterator()
    images_next = images_iter.get_next()

    labels = tf.data.Dataset.from_generator(generator.get_labels,
                                        tf.float32,
                                        tf.TensorShape([2])).batch(batch_size)
    labels_iter = labels.make_initializable_iterator()
    labels_next = labels_iter.get_next()

    return (images_iter,images_next,labels_iter,labels_next)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='DICOM to HDF5')
    parser.add_argument('-i', dest='dicom_input',help='Input Filepath of DICOM Locations',required=True)
    parser.add_argument('-o', dest='output_path',help='Output Filepath for Generated HDF5',required=True)
    parser.add_argument('-t', dest='number_of_threads',help='Number of threads',default=1,type=int)
    args = parser.parse_args()

    if args.number_of_threads > 1:
        DICOM_HDF5_Converter.threaded_preprocess_data(args.dicom_input,
                                                  args.output_path,
                                                  args.number_of_threads)
    else:
        DICOM_HDF5_Converter.single_threaded_preprocess_data(args.dicom_input,
                                                 args.output_path)
