import tensorflow as tf
import numpy as np
import os
import argparse
import h5py
import pydicom
from cw_cnn import inception_resnet_resize,inception_resnet_two_layer_resize
from dicom_to_hdf5 import create_dataset

model_dict = {'resize': inception_resnet_resize,
              'two_layer': inception_resnet_two_layer_resize}

#------------------------------------------------------------------------------------------
# FUNCTION: predict(input_path, model_architecture, model_checkpoint, batch_size)
# OVERVIEW: Perform inference on a dataset using a model checkpoint
# INPUTS:
#       input_path: HDF5 File Path of Dataset
#       model_architecture: Neural Network Architecture (resize or two_layer)
#       model_checkpoint: Model Checkpoint Filepath
#       batch_size: Number of datapoints per batch
# OUTPUTS: Predictions and their corresponding labels
#------------------------------------------------------------------------------------------
def predict(input_path, model_architecture, model_checkpoint, batch_size):
    images_iter,images_next,labels_iter,labels_next = create_dataset(input_path,batch_size)
    x,y,_,logits,is_training = model_dict[model_architecture]()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        sess.run([images_iter.initializer,labels_iter.initializer])
        saver.restore(sess,model_checkpoint)
        predictions = np.empty((0,2),np.float32)
        labels = np.empty((0,2),np.float32)
        try:
            while True:
                batch_images,batch_labels = sess.run([images_next,labels_next])
                pred = sess.run(logits,feed_dict={x:batch_images,is_training:False})
                pred = (pred*127.5)+127.5
                predictions = np.vstack((predictions,pred))
                labels = np.vstack((labels,batch_labels))
        except tf.errors.OutOfRangeError:
            pass
    return (predictions,labels)

#------------------------------------------------------------------------------------------
# FUNCTION: record_predictions_for_transformation(dicom_list_path,
#                                                 input_hdf5,
#                                                 output_path,
#                                                 model_architecture,
#                                                 model_checkpoint,
#                                                 batch_size)
# OVERVIEW: Prepares required data for PNG generation
# INPUTS:
#       dicom_list_path: Path to the list of dicoms
#       input_hdf5: Path to HDF5 file that was generated from dicom_list
#       output_path: Path to output file used for generating PNGs
#       model_architecture: Neural Network Architecture (resize or two_layer)
#       model_checkpoint: Model Checkpoint Filepath
#       batch_size: Number of datapoints per batch
# OUTPUTS: A file containing predicted center and width values along with its associated
# DICOM path
#------------------------------------------------------------------------------------------
def record_predictions_for_transformation(dicom_list_path,input_hdf5,output_path,model_architecture,model_checkpoint,batch_size):

    with open(dicom_list_path) as f:
        dicom_list = [line.strip() for line in f]

    indices = h5py.File(input_hdf5)['index'][...]
    dicom_list = [dicom_list[i] for i in indices]
    predictions,labels = predict(input_hdf5,model_architecture,model_checkpoint,batch_size)
    with open(output_path,'w') as f:
        for i in range(len(dicom_list)):
            dcm = pydicom.dcmread(dicom_list[i])
            bitsStored = dcm.BitsStored
            if bitsStored == 16:
                print('Bitstored is 16')
                assert dcm.LargestImagePixelValue == 4095
                bitsStored = 12
            rescaled_prediction = predictions[i]*(2**(bitsStored)-1)/255.0
            rescaled_labels = labels[i]*(2**(bitsStored)-1)/255.0
            f.write(dicom_list[i] + ',' +
                   str(rescaled_prediction[0]) + ',' +
                   str(rescaled_prediction[1]) + ',' +
                   str(rescaled_labels[0]) + ',' +
                   str(rescaled_labels[1]) + '\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CWCNN Inference')

    parser.add_argument('-d', dest='dicom_list_path', help='DICOM List File Path', required=True)
    parser.add_argument('-i', dest='input_path',help='HDF5 Filepath of Input Data',required=True)
    parser.add_argument('-o', dest='output_path',help='Output Path',required=True)
    parser.add_argument('-g', dest='gpu_num', help='GPU Number', required=True)
    parser.add_argument('-m', dest='model_type',help='Model Architecture: resize or two_layer',required=True)
    parser.add_argument('-c', dest='checkpoint_path',help='Checkpoint Path',required=True)
    parser.add_argument('-b', dest='batch_size',help='Batch Size', required=True,type=int)
    args = parser.parse_args()

    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_num

    record_predictions_for_transformation(args.dicom_list_path,
                       args.input_path,
                       args.output_path,
                       args.model_type,
                       args.checkpoint_path,
                       args.batch_size)
